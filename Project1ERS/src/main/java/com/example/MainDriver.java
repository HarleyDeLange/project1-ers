package com.example;

import java.io.File;
import java.time.LocalDate;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.example.controller.ReimbController;
import com.example.controller.UserController;
import com.example.dao.ERSDBConnection;
import com.example.dao.ReimbursementDaoImpl;
import com.example.dao.UserDaoImpl;
import com.example.service.ReimbursementService;
import com.example.service.UserService;

import io.javalin.Javalin;

public class MainDriver {

	public static void main(String[] args) {
		
		ReimbController rCon = new ReimbController(new ReimbursementService(new ReimbursementDaoImpl(new ERSDBConnection())));

		UserController uCon = new UserController(new UserService(new UserDaoImpl(new ERSDBConnection())));

		Javalin app = Javalin.create(config -> {
			config.enableCorsForAllOrigins();
			config.addStaticFiles("/frontend");
		});

		app.start(9022);

		app.post("/user/login", uCon.POSTLOGIN);		

		app.post("/emp/reimbsubmit", rCon.NEWRIMB);
		
		app.post("/ersman/update_reimb", rCon.UPDATERIMB);
		
		app.get("/user/sessuser", uCon.GETCURRENTSUSER);

        app.get("/reimbemp/pastreimb", rCon.GETBYAUTHOR);
        
        app.get("/reimbman/allreimb", rCon.GETALLREIMB);
        
        app.get("/reimbman/pendreimb", rCon.GETBYSTATUS);
        
        app.get("/reimbemp/pendreimb", rCon.GETPENDINGEMP);       
        

		app.exception(NullPointerException.class, (e, ctx) -> {
			ctx.status(404);
			ctx.redirect("/html/invalidlogin.html");
		});

		File file = new File("src/main/resources/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		WebDriver driver = new ChromeDriver();
		driver.get("http://localhost:9022/html/");
	}

}























//TESTS
//ReimbursementDaoImpl reimb = new ReimbursementDaoImpl(new ERSDBConnection());
//reimb.createNewReimb(200.9, LocalDate.now(), "Hi there", 5, 1, 2);
//
//ReimbursementDaoImpl reimb2 = new ReimbursementDaoImpl(new ERSDBConnection());
//reimb2.updateReimbursement(4, LocalDate.now(), 6, 1);
//

//ReimbursementDaoImpl r4 = new ReimbursementDaoImpl(new ERSDBConnection());
//System.out.println(r4.viewAllReimbursementsByAuthorId(1));
//
//ReimbursementDaoImpl reimb4 = new ReimbursementDaoImpl(new ERSDBConnection());
//System.out.println(reimb4.viewAllReimbursements());
//
//UserDaoImpl userdaoimpl = new UserDaoImpl(new ERSDBConnection());
//userdaoimpl.getUserByUsername("Hdelange");
//userdaoimpl.getRoleId(userdaoimpl.getUserByUsername("Hdelange"));