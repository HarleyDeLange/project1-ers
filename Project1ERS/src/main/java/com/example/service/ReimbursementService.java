package com.example.service;

import java.time.LocalDate;
import java.util.List;

import com.example.dao.ReimbursementDaoImpl;
import com.example.model.Reimbursement;

public class ReimbursementService {
	
	private ReimbursementDaoImpl rDao;

    public ReimbursementService() {
        // TODO Auto-generated constructor stub
    }

    public ReimbursementService(ReimbursementDaoImpl rDao) {
        super();
        this.rDao = rDao;
    }
    
    public Integer cTypeId(String typeId) {
        int type_Id = 0;
        if(typeId.equals("apparel")) {
            type_Id = 1;
        } else if(typeId.equals("food")) {
            type_Id = 2;
        } else if(typeId.equals("lodging")) {
            type_Id = 3;
        } else if(typeId.equals("travel")) {
            type_Id = 4;
        }
        return type_Id;
    }
    
    public Integer cStatusId(String status_id) {
        int statusId = 0;
        if(status_id.equals("pending")) {
            statusId = 1;
        } else if (status_id.equals("approved")) {
            statusId = 2;
        } else if (status_id.equals("denied")) {
            statusId = 3;
        }
        return statusId;
    }
    
    public void newReimb1(double amount, LocalDate submitted, String description, int authorId, int type_id) {
        rDao.createNewReimb1(amount, submitted, description, authorId, type_id);
    }
    
    public void updateReimbursement(int reimbursement_id, LocalDate resolved, int resolver_id, int status_id) {
    	rDao.updateReimbursement(reimbursement_id, resolved, resolver_id, status_id);
    }
    
    public List<Reimbursement> getReimbsByAID(int authorid) {
        return rDao.viewAllReimbursementsByAuthorId(authorid);
    }
    
    public List<Reimbursement> viewAllReimbs() {
        return rDao.viewAllReimbursements();
    }
    
    public List<Reimbursement> getPendingReimbursementsByAuthorid(int authorid) {
        return rDao.getPendingReimbursementsByAuthorid(authorid);
    }

    public List<Reimbursement> getReimbByStatus() {
        return rDao.getAllReimbursementsByStatus();
    }

    

}






