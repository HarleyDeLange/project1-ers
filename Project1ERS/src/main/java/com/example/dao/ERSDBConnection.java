package com.example.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ERSDBConnection {

	private static final String URL = "jdbc:postgresql://jwa-db.cuhtbxz9fbmr.us-east-2.rds.amazonaws.com/project1";
	private static final String username = "manager";
	private static final String password = "P4ssw0rd";

	public Connection getDBConnection() throws SQLException {
		return DriverManager.getConnection(URL, username, password);
	}

}


