package com.example.dao;

import java.time.LocalDate;
import java.util.List;

import com.example.model.Reimbursement;

public interface ReimbursementDao {
	
	void createNewReimb1(double amount, LocalDate submitted, String description, int authorId, int type_id);	
	
	void updateReimbursement(int reimbursement_id, LocalDate resolved, int resolver_id, int status_id);
	
	List<Reimbursement> viewAllReimbursements();
	
	List<Reimbursement> viewAllReimbursementsByAuthorId(int authorid);
	    
    List<Reimbursement> getPendingReimbursementsByAuthorid(int authorid);
    
    List<Reimbursement> getAllReimbursementsByStatus();

}

