package com.example.controller;

import com.example.model.User;
import com.example.service.UserService;

import io.javalin.http.Handler;

public class UserController {

	private UserService uServ;

	public UserController() {
		
	}

	public UserController(UserService uServ) {
		super();
		this.uServ = uServ;
	}

	public final Handler POSTLOGIN = (ctx) -> {
		User user = uServ.checkPassword(ctx.formParam("username"), ctx.formParam("pass"));
		if (user != null & user.getUser_role_id() == 1) {
			ctx.sessionAttribute("currentuser", user);
			ctx.redirect("/html/home_emp.html");
		} else if (user != null & user.getUser_role_id() == 2) {
			ctx.sessionAttribute("currentuser", user); 
			ctx.redirect("/html/home_man.html");
		} else {
			ctx.redirect("/html/invalidlogin.html");
		}
	};

	public final Handler GETCURRENTSUSER = (ctx) -> {
		User user = (User) ctx.sessionAttribute("currentuser");
		ctx.json(user);
	};

}