package com.example.controller;

import java.time.LocalDate;
import java.util.List;

import com.example.model.Reimbursement;
import com.example.model.User;
import com.example.service.ReimbursementService;
import com.example.service.UserService;

import io.javalin.http.Handler;

public class ReimbController {
	private ReimbursementService rSer;
	private UserService uSer;

	public ReimbController() {
		// TODO Auto-generated constructor stub
	}

	public ReimbController(ReimbursementService rSer) {
		super();
		this.rSer = rSer;
	}
	
	public final Handler NEWRIMB = (ctx) -> {
        User sessuser = (User)ctx.sessionAttribute("currentuser");
        double amount = Double.parseDouble(ctx.formParam("amount"));
        LocalDate submitdate = LocalDate.now();
        String description = ctx.formParam("description");
        int authorId = sessuser.getUser_id();
        int typeid = rSer.cTypeId(ctx.formParam("type_id"));
        rSer.newReimb1(amount, submitdate, description, authorId, typeid);
        ctx.status(200);
        ctx.redirect("/html/home_emp.html");
    };

    
    public final Handler UPDATERIMB = (ctx) -> {
        User sessuser = (User)ctx.sessionAttribute("currentuser");
        int rID = Integer.parseInt(ctx.formParam("reimbursement_id"));
        LocalDate resolved_Date = LocalDate.parse(ctx.formParam("resolved"));
        int resolverId = sessuser.getUser_id();
        int status_id = rSer.cStatusId(ctx.formParam("status_id"));
        rSer.updateReimbursement(rID, resolved_Date, resolverId, status_id);
        ctx.status(200);
        ctx.redirect("/html/home_man.html");
    };
    
    public final Handler GETBYAUTHOR = (ctx) -> {
        User sessuser = (User)ctx.sessionAttribute("currentuser");
        int authorid = sessuser.getUser_id();
        ctx.status(200);
        ctx.json(rSer.getReimbsByAID(authorid));

    };

    public final Handler GETALLREIMB = (ctx) -> {
        ctx.status(200);
        ctx.json(rSer.viewAllReimbs());
    };
    
    public final Handler GETPENDINGEMP = (ctx) -> {
        User sessuser = (User)ctx.sessionAttribute("currentuser");
        int authorid = sessuser.getUser_id();
        ctx.status(200);
        ctx.json(rSer.getPendingReimbursementsByAuthorid(authorid));

    };
    
    public final Handler GETBYSTATUS = (ctx) -> {
        ctx.status(200);
        ctx.json(rSer.getReimbByStatus());

    };


}
	

