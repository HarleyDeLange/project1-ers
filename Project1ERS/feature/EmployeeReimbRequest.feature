
Feature: Employee submitting a new reimbursement request
	As a employee, I wish to submit a new request with the proper credentials
	
	Scenario: Employee will chose to submit reimbursement request 
		Given a user is logged in as an employee 
		When a user inputs a reimbursment type and amount
		And they provided a description
		And they provided an amount
		Then a user has submitted a reimbursement request