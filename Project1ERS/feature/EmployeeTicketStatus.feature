
Feature: Employee viewing their past and pending tickets
  As a employee, I wish to view my past tickets and current pending tickets

	Scenario: Employee will chose to view tickets 
		Given a user is logged in as an employee
		When a user choses to view tickets
		Then the users past tickets will be displayed
		And the users present tickets will also be displayed
