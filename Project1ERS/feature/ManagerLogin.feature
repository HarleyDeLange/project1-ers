
Feature: Manager Logging in to ERS App
  As a Manager, I wish to login to the ERS App using proper Credentials

  Scenario: Successful Login to the ERS App
    Given a user is at the login page 
    When a user inputs their user name "<username>"
    And a user iputs their password "<password>"
    And a user submits the information 
    But that user is a user of role Manager
    Then the user is redirected to the Manager ERS Page
    
    Examples:
    			| username | password | 
    			| manager  | manager  | 