#Include an approval file


Feature: Employee Logging in to ERS App
  As a Employee, I wish to login to the ERS App using proper Credentials

  Scenario: Successful Login to the ERS App
    Given a user is at the login page 
    When a user inputs their user name "<username>"
    And a user inputs their password "<password>"
    And a user submits the information 
    But that user is a user of role Employee
    Then the user is redirected to the Employee ERS Page
    
    Examples:
    			| username  | password  |
    			| employee1 | employee1 |
	
	Scenario: Failing to Login to Employee ERS Page
		Given a user is at the login page
		When a user incorrectly inputs their user name
		And a user incorrectly inputs their password
		But a user submits the information
		Then the user should see the span of Invalid username or password
		
	Scenario: Failing to Login to Employee ERS Page 2
		Given a user is at the login page
		When a user correctly inputs their user name
		And a user incorrectly inputs their password
		But a user submits the information
		Then the user should see the span of Invalid username or password
    			

		
	Scenario: Employee will chose to view past tickets 
		Given a user is logged in as an employee
		When a user chose to view past tickets
		Then the users past tickets will be displayed
		
		
